package com.paulmarkcastillo.android2e2.common.configs;

import io.appium.java_client.remote.AndroidMobileCapabilityType;

public class ConfigAndroid extends BaseAppiumConfig {

    public String getAdbPort() {
        return getEnvironment(AndroidMobileCapabilityType.ADB_PORT);
    }

    public String getAppPackage() {
        return "com.paulmarkcastillo.androide2e"; //TODO remove hardcoded value
    }

    public String getAppActivity() {
        return "com.paulmarkcastillo.androide2e.MainActivity"; //TODO remove hardcoded value
    }

    public String getAutoGrantPermissions() {
        return getEnvironment(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS);
    }

    public String getSystemPort() {
        return getEnvironment(AndroidMobileCapabilityType.SYSTEM_PORT);
    }
}
