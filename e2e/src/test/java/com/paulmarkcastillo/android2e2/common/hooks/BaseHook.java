package com.paulmarkcastillo.android2e2.common.hooks;

import com.paulmarkcastillo.android2e2.common.configs.ConfigAndroid;

import org.openqa.selenium.WebDriver;

public class BaseHook {

    @SuppressWarnings("rawtypes")
    public static WebDriver driver;
    public static ConfigAndroid config = new ConfigAndroid();
}
