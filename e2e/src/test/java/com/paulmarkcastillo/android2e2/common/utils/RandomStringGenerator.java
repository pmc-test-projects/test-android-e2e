package com.paulmarkcastillo.android2e2.common.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Random;

import me.xdrop.jrand.JRand;
import me.xdrop.jrand.generators.basics.NaturalGenerator;
import me.xdrop.jrand.generators.person.FirstnameGenerator;
import me.xdrop.jrand.generators.person.LastnameGenerator;
import me.xdrop.jrand.generators.person.NameGenerator;
import me.xdrop.jrand.generators.text.SentenceGenerator;
import me.xdrop.jrand.generators.text.WordGenerator;

public class RandomStringGenerator {

    public static String generateUniqueEmail(String email) {
        return email + "+" + generateDateTimeFormat() + "@isbx.com";
    }

    public static String generateUniqueEmail(String email, String lastName) {
        return email + "+" + lastName + "+" + generateDateTimeFormat() + "@isbx.com";
    }

    private static String generateDateTimeFormat() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'-HH-mm-ss-SSS");
        LocalDateTime currentDateAndTime = LocalDateTime.now();

        return dtf.format(currentDateAndTime);
    }

    public static String generatePassword(String firstName) {
        Random random = new Random();
        String randomStringAndNumber = String.format("%04d", random.nextInt(9999));

        return firstName.replaceAll("\\s+", "") + randomStringAndNumber;
    }

    public static String generateFirstName() {
        FirstnameGenerator firstNameGenerator = JRand.firstname();
        return firstNameGenerator.gen() + " " + firstNameGenerator.gen();
    }

    public static String generateLastName() {
        LastnameGenerator lastNameGenerator = JRand.lastname();
        return lastNameGenerator.gen();
    }

    public static String generateFullName() {
        NameGenerator fullNameGenerator = JRand.name().withMiddleName();
        return fullNameGenerator.gen();
    }

    public static String generateMemberId() {
        WordGenerator wordGenerator = JRand.word();
        NaturalGenerator naturalGenerator = JRand.natural().min(1000).max(9999);
        return String.format("%s%s", wordGenerator.gen(), naturalGenerator.gen());
    }

    public static String wrapFirstNameWithHTMLTags(String firstName) {
        String[] splitFirstName = firstName.split(" ");
        splitFirstName[0] = "<b><i>" + splitFirstName[0] + "</i></b>";
        return Arrays.toString(splitFirstName).replace("[", "").replace(",", "").replace("]", "");
    }

    public static String generateSentence() {
        SentenceGenerator sentence = JRand.sentence();
        return sentence.gen();
    }
}
