package com.paulmarkcastillo.android2e2.common.configs;

public abstract class BaseCommonConfig {

    public static String getEnvironment(String name) {
        String value = System.getProperty(name);
        if (value == null) {
            System.out.println(name + " is empty.");
        }
        return value;
    }

    public long getImplicitlyWait() {
        return 3L; //TODO remove hardcoded value
    }
}
