package com.paulmarkcastillo.android2e2.common.utils;

import static org.junit.Assert.assertEquals;

public class AssertionHelper {

    public static void assertStringEqualsIgnoreCase(String expected, String actual) {
        assertEquals(expected.toLowerCase(), actual.toLowerCase());
    }
}
