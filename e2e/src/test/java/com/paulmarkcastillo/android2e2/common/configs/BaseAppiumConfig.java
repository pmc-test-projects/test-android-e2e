package com.paulmarkcastillo.android2e2.common.configs;

import io.appium.java_client.remote.MobileCapabilityType;

public abstract class BaseAppiumConfig extends BaseCommonConfig {

    // ==========  Standard Configs

    public String getAutomationName() {
        return getEnvironment(MobileCapabilityType.AUTOMATION_NAME);
    }

    public String getPlatformName() {
        return getEnvironment("platformName");
    }

    public String getPlatformVersion() {
        return getEnvironment(MobileCapabilityType.PLATFORM_VERSION);
    }

    public String getDeviceName() {
        return "Pixel 2 API 28 (With Google Play)"; //TODO remove hardcoded value
    }

    public String getApp() {
        return "../app/build/outputs/apk/debug/app-debug.apk"; //TODO remove hardcoded value
    }

    public String getUdid() {
        return getEnvironment(MobileCapabilityType.UDID);
    }

    public String getNewCommandTimeout() {
        return getEnvironment(MobileCapabilityType.NEW_COMMAND_TIMEOUT);
    }

    // ==========  Other Configs

    public String appiumURL() {
        return getEnvironment("appiumURL");
    }

    public boolean isKeypadDisabled() {
        String disableKeypad = getEnvironment("keypadDisabled");
        return disableKeypad != null && disableKeypad.equals("true");
    }
}
