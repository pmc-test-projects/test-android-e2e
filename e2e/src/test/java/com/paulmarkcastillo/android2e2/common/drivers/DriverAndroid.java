package com.paulmarkcastillo.android2e2.common.drivers;

import com.paulmarkcastillo.android2e2.common.configs.BaseCommonConfig;
import com.paulmarkcastillo.android2e2.common.configs.ConfigAndroid;
import com.paulmarkcastillo.android2e2.hooks.android.HooksAndroid;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class DriverAndroid {

    private ConfigAndroid config = new ConfigAndroid();

    @SuppressWarnings("rawtypes")
    public void mobileDevice() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        String port = "54"; //TODO remove hardcoded value

        // ========== Standard Configs

        String automationName = "UiAutomator2";
        System.out.println("[CONFIG] " + MobileCapabilityType.AUTOMATION_NAME + ": " + automationName);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);

        String deviceName = config.getDeviceName();
        System.out.println("[CONFIG] " + MobileCapabilityType.DEVICE_NAME + ": " + deviceName);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);

        String udid = "emulator-55" + port;
        System.out.println("[CONFIG] " + MobileCapabilityType.UDID + ": " + udid);
        capabilities.setCapability(MobileCapabilityType.UDID, udid);

        boolean installed = false;
        String installedStr = BaseCommonConfig.getEnvironment("installed");
        if (installedStr != null && installedStr.equals("true")) {
            installed = true;
        }
        System.out.println("[CONFIG] installed: " + installed);

        if (!installed) {
            String app = config.getApp();
            System.out.println("[CONFIG] " + MobileCapabilityType.APP + ": " + app);
            capabilities.setCapability(MobileCapabilityType.APP, app);

            boolean fullReset = true;
            System.out.println("[CONFIG] " + MobileCapabilityType.FULL_RESET + ": " + fullReset);
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, fullReset);
        }

        int newCommandTimeout = 60;
        System.out.println("[CONFIG] " + MobileCapabilityType.NEW_COMMAND_TIMEOUT + ": " + newCommandTimeout);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);

        // ========== Android Configs

//        String adbPort = config.getAdbPort();
//        if (adbPort == null || adbPort.equals("")) {
//            adbPort = "50" + port;
//        }
//        capabilities.setCapability(AndroidMobileCapabilityType.ADB_PORT, adbPort);
//        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.ADB_PORT + ": " + adbPort);

        String appPackage = config.getAppPackage();
        if (appPackage != null && !appPackage.equals("")) {
            System.out.println("[CONFIG] " + AndroidMobileCapabilityType.APP_PACKAGE + ": " + appPackage);
            capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appPackage);
        }

        String appActivity = config.getAppActivity();
        if (appActivity != null && !appActivity.equals("")) {
            System.out.println("[CONFIG] " + AndroidMobileCapabilityType.APP_ACTIVITY + ": " + appActivity);
            capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, appActivity);
        }

        boolean allowTestPackages = true;
        System.out.println("[CONFIG] allowTestPackages: " + allowTestPackages);
        capabilities.setCapability("allowTestPackages", allowTestPackages);

        boolean autoGrantPermissions = true;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS + ": " + autoGrantPermissions);
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, autoGrantPermissions);

        String systemPort = "82" + port;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.SYSTEM_PORT + ": " + systemPort);
        capabilities.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, Integer.parseInt(systemPort));

        int appWaitDuration = 60000;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.APP_WAIT_DURATION + ": " + appWaitDuration);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_DURATION, appWaitDuration);

        int deviceReadyTimeout = 60;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.DEVICE_READY_TIMEOUT + ": " + deviceReadyTimeout);
        capabilities.setCapability(AndroidMobileCapabilityType.DEVICE_READY_TIMEOUT, deviceReadyTimeout);

        int androidDeviceReadyTimeout = 60;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.ANDROID_DEVICE_READY_TIMEOUT + ": " + androidDeviceReadyTimeout);
        capabilities.setCapability(AndroidMobileCapabilityType.ANDROID_DEVICE_READY_TIMEOUT, androidDeviceReadyTimeout);

        int androidInstallTimeout = 90000;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.ANDROID_INSTALL_TIMEOUT + ": " + androidInstallTimeout);
        capabilities.setCapability(AndroidMobileCapabilityType.ANDROID_INSTALL_TIMEOUT, androidInstallTimeout);

        int avdLaunchTimeout = 60000;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.AVD_LAUNCH_TIMEOUT + ": " + avdLaunchTimeout);
        capabilities.setCapability(AndroidMobileCapabilityType.ANDROID_INSTALL_TIMEOUT, avdLaunchTimeout);

        int avdReadyTimeout = 120000;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.AVD_READY_TIMEOUT + ": " + avdReadyTimeout);
        capabilities.setCapability(AndroidMobileCapabilityType.AVD_READY_TIMEOUT, avdReadyTimeout);

        int autoWebviewTimeout = 60000;
        System.out.println("[CONFIG] " + AndroidMobileCapabilityType.AUTO_WEBVIEW_TIMEOUT + ": " + autoWebviewTimeout);
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_WEBVIEW_TIMEOUT, autoWebviewTimeout);

        int adbExecTimeout = 60000;
        System.out.println("[CONFIG] " + "adbExecTimeout" + ": " + adbExecTimeout);
        capabilities.setCapability("adbExecTimeout", adbExecTimeout);

        // ========== UIAutomator2 Configs

        int uiautomator2ServerLaunchTimeout = 60000;
        System.out.println("[CONFIG] " + "uiautomator2ServerLaunchTimeout" + ": " + uiautomator2ServerLaunchTimeout);
        capabilities.setCapability("uiautomator2ServerLaunchTimeout", uiautomator2ServerLaunchTimeout);

        int uiautomator2ServerInstallTimeout = 60000;
        System.out.println("[CONFIG] " + "uiautomator2ServerInstallTimeout" + ": " + uiautomator2ServerInstallTimeout);
        capabilities.setCapability("uiautomator2ServerInstallTimeout", uiautomator2ServerInstallTimeout);

        // ========== Other Configs

        HooksAndroid.driver = new AndroidDriver<AndroidElement>(
                new URL(BaseCommonConfig.getEnvironment("remoteAddress")),
                capabilities);
    }
}
