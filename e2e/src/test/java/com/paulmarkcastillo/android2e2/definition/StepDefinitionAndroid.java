package com.paulmarkcastillo.android2e2.definition;

import com.paulmarkcastillo.android2e2.hooks.android.HooksAndroid;
import com.paulmarkcastillo.android2e2.screens.MainScreen;

import cucumber.api.java.en.Given;

public class StepDefinitionAndroid {

    private MainScreen appiumUnitTest = new MainScreen(HooksAndroid.driver);

    @Given("that the Main activity screen has been displayed")
    public void mainActivityScreenHasBeenDisplayed() {
        appiumUnitTest.testAppium();
    }
}
