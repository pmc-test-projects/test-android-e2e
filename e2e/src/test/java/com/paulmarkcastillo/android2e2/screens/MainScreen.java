package com.paulmarkcastillo.android2e2.screens;

import com.paulmarkcastillo.android2e2.base.BaseScreen;
import com.paulmarkcastillo.seleniumtoolbox.utils.LogUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class MainScreen extends BaseScreen {

    public MainScreen(WebDriver driver) {
        super((AndroidDriver<AndroidElement>) driver);
    }

    public void testAppium() {
        AndroidElement androidElement = (AndroidElement) findUtils.findElement("Hello World!", By.id("textview_hello"));
        LogUtils.stepPassed("Successfully displayed the Main Activity Screen");
    }
}
