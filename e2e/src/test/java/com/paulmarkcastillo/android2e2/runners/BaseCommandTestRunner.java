package com.paulmarkcastillo.android2e2.runners;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class BaseCommandTestRunner {
    protected static int runCommand(String command) throws Exception {
        System.out.println("Running Command: " + command);
        final Process process = Runtime.getRuntime().exec(command);
        new Thread(() -> {
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            try {
                while ((line = input.readLine()) != null)
                    System.out.println(line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        process.waitFor();
        return process.exitValue();
    }
}
