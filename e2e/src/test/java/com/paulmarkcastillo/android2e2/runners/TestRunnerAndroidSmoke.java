package com.paulmarkcastillo.android2e2.runners;

import com.paulmarkcastillo.android2e2.common.utils.AppiumServiceUtils;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.File;
import java.net.URL;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/com/paulmarkcastillo/android2e2/feature",
        glue = {"com.paulmarkcastillo.android2e2.definition", "com.paulmarkcastillo.android2e2.hooks.android"},
        plugin = {
                "pretty",
                "html:target/cucumber-reports/html",
                "json:target/cucumber-reports/json/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:target/cucumber-reports/android/StayHealthyAndroidSmokeExtentReport.html"
        },
        monochrome = true,
        dryRun = false,
        tags = {"@SmokeTest"}
)
public class TestRunnerAndroidSmoke {

    @BeforeClass
    public static void onStart() {
        String logPath = System.getProperty("user.dir") + "/target/appium.log";
        File logFile = new File(logPath);

        int port = 54; //TODO remove hardcoded value

        String appiumPort = "47" + port;
        String bootstrapPort = "47" + (port + 1);

        URL url = AppiumServiceUtils.startServer(
                logFile,
                Integer.parseInt(appiumPort),
                Integer.parseInt(bootstrapPort));

        System.setProperty("remoteAddress", url.toString());
    }

    @AfterClass
    public static void onStop() {
        AppiumServiceUtils.stopServer();
    }
}
