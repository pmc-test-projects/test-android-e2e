package com.paulmarkcastillo.android2e2.base;

import com.paulmarkcastillo.android2e2.hooks.android.HooksAndroid;
import com.paulmarkcastillo.seleniumtoolbox.utils.FindUtils;
import com.paulmarkcastillo.seleniumtoolbox.utils.KeyboardUtils;
import com.paulmarkcastillo.seleniumtoolbox.utils.SwipeUtils;
import com.paulmarkcastillo.seleniumtoolbox.utils.TapUtils;

import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public abstract class BaseScreen {

    protected AndroidDriver<AndroidElement> driver;
    protected FindUtils findUtils;
    protected KeyboardUtils keyboardUtils;
    protected SwipeUtils swipeUtils;
    protected TapUtils tapUtils;
    protected WebDriverWait webDriverWait;

    public BaseScreen(AndroidDriver<AndroidElement> driver) {
        this.driver = driver;
        this.findUtils = new FindUtils(driver, 60, HooksAndroid.config.isKeypadDisabled());
        this.keyboardUtils = new KeyboardUtils(driver, driver, HooksAndroid.config.isKeypadDisabled());
        this.swipeUtils = new SwipeUtils(driver);
        this.tapUtils = new TapUtils(driver);
        this.webDriverWait = new WebDriverWait(driver, 10);
    }

    protected void sendKeys(AndroidElement androidElement, CharSequence... keysToSend) {
        keyboardUtils.hideKeyboard();
        androidElement.sendKeys(keysToSend);
        keyboardUtils.hideKeyboard();
    }
}
