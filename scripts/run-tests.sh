#!/usr/bin/env bash

./gradlew \
    --no-daemon \
    --warning-mode all \
    clean \
    assembleDebug \
    testDebugUnitTest \
    connectedDebugAndroidTest

EXIT_CODE=$?
if [[ ${EXIT_CODE} -gt 0 ]]; then
    exit ${EXIT_CODE}
fi

./gradlew \
    --no-daemon \
    --warning-mode all \
    e2e:test

EXIT_CODE=$?
if [[ ${EXIT_CODE} -gt 0 ]]; then
    exit ${EXIT_CODE}
fi